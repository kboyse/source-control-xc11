//
//  AppDelegate.swift
//  Source Control
//
//  Created by Kevin Boyse on 4/30/20.
//  Copyright © 2020 Kevin Boyse. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {



    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }


}

